extends Node2D

export var watcher = ""
export var trail_color = Color(1, 0, 0, 128.0/255)

var sprites = []
var watch_obj
var current_animation
var watched_positions = []
var watched_animations = []

var container

const TRAIL_LENGTH = 8 # How many sprites in the trail should be displayed

# This class adds trails behind the characters. I wanted to use Particles2D for this, but it doesn't currently accept sprite sheets...
# This provides more granular control anyways.

# This works by sampling the last few positions the character has moved into. I felt it was easiest this way.
func _ready():
	watch_obj = get_node(watcher)
	container = get_node("trail")
	watched_positions = []
	current_animation = watch_obj.get("current_animation")
	set_physics_process(true)

func _exit_tree():
	watched_positions = []
	clear_sprites()

func _physics_process(delta):
	var newanimation = watch_obj.get("current_animation")
	var is_static_animation = newanimation == "transform" || newanimation == "detransform" || newanimation == "gameover"
	if (!is_static_animation):
		watched_positions.push_front(watch_obj.global_position)
	if (watched_positions.size() > TRAIL_LENGTH * TRAIL_LENGTH):
		watched_positions.pop_back()
	if (!is_static_animation && sprites.size() < TRAIL_LENGTH):
		add_sprite()
	if (current_animation != newanimation):
		current_animation = newanimation
	if (sprites.size() >= TRAIL_LENGTH || is_static_animation):
		clear_sprite(sprites.pop_back())
		watched_animations.pop_back()
	process_sprites()

func find_sprite_ref(animation=current_animation):
	var sprite = get_node(animation)
	if (!sprite.is_class("Sprite")):
		if (sprite.has_node(animation)):
			return sprite.get_node(animation)
		if (sprite.has_node("attack")):
			return sprite.get_node("attack")
		return sprite.get_node("aattack")
	return sprite

func add_sprite():
		var sprite = find_sprite_ref().duplicate()
		sprite.visible = true
		sprite.modulate = trail_color
		#sprite.set_material(CanvasItemMaterial.new())
		#sprite.get_material().blend_mode = BLEND_MODE_ADD
		container.add_child(sprite)
		watched_animations.push_front({"animation": current_animation, "direction": watch_obj.get("direction")})
		sprites.push_front(sprite)

func clear_sprites():
	for sprite in sprites:
		clear_sprite(sprite)
	sprites = []

func clear_sprite(sprite):
	if (sprite != null):
		container.remove_child(sprite)
		sprite.queue_free()

func process_sprites():
	if (watched_positions.size() < 1):
		watched_positions.push_back(watch_obj.global_position)
	for i in sprites.size():
		var position_index = min(watched_positions.size() - 1, i * i)
		var watched_position = watched_positions[position_index]
		var sprite = sprites[i]
		var sprite_ref = find_sprite_ref(watched_animations[i].animation)
		if (is_instance_valid(sprite)):
			sprite.scale.x = watched_animations[i].direction
			sprite.global_position = Vector2(sprite_ref.position.x * sprite.scale.x, sprite_ref.position.y) + watched_position
			sprite.modulate.a = (float(watched_positions.size() - position_index - 1) / watched_positions.size()) * (128.0/255)
