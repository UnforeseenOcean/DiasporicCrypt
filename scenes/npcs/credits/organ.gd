extends Node2D

var bgmselectorclass = preload("res://gui/menu/bgm.tscn")

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var music

# Called when the node enters the scene tree for the first time.
func _ready():
	music = get_tree().get_root().get_node("world/music")
	music.stop()
	ProjectSettings.set("bgmselection", null)

func start(pos):
	var bgminstance = bgmselectorclass.instance()
	bgminstance.set_position(Vector2(32, 20))
	#hide_dialog()
	get_tree().set_pause(true)
	var pause = get_tree().get_root().get_node("world/gui/CanvasLayer/pause")
	pause.get_node("menu").hide()
	pause.add_child(bgminstance)
	pause.show()

func enter_screen():
	#set_physics_process(true)
	pass

func exit_screen():
	#set_physics_process(false)
	pass
