extends "res://scenes/npcs/BaseNPC.gd"

export var character = ""

var characters = {
	"Kaleva": {"small": preload("res://scenes/npcs/credits/frames/kaleva.png"), "full": preload("res://gui/portraits/kaleva.png"), "creator": "beelzy"},
	"Lucifer": {"small": preload("res://scenes/npcs/credits/frames/lucifer.png"), "full": preload("res://gui/portraits/lucifer.png"), "creator": "beelzy"},
	"Gareth": {"small": preload("res://scenes/npcs/credits/frames/gareth.png"), "full": preload("res://gui/portraits/gareth.png"), "creator": "beelzy"},
	"Gabriel": {"small": preload("res://scenes/npcs/credits/frames/gabriel.png"), "full": preload("res://gui/portraits/gabriel.png"), "creator": "beelzy"},
	"Jalo": {"small": preload("res://scenes/npcs/credits/frames/jalo.png"), "full": preload("res://gui/portraits/jalo.png"), "creator": "beelzy"},
	"Vance": {"small": preload("res://scenes/npcs/credits/frames/vance.png"), "full": preload("res://gui/portraits/vance.png"), "creator": "beelzy"},
	"Vladimir": {"small": preload("res://scenes/npcs/credits/frames/vladimir.png"), "full": preload("res://gui/portraits/vladimir.png"), "creator": "beelzy"},
	"Pepper": {"small": preload("res://scenes/npcs/credits/frames/peppercarrot.png"), "full": preload("res://gui/portraits/peppercarrot.png"), "creator": "David Revoy"},
	"Goddess": {"small": preload("res://scenes/npcs/credits/frames/goddess.png"), "full": preload("res://gui/portraits/goddess.png"), "creator": "Anna Dorokhova"},
	"Nystev": {"small": preload("res://scenes/npcs/credits/frames/nystev.png"), "full": preload("res://gui/portraits/nystev.png"), "creator": "Carbonoid"},
	"Taevica": {"small": preload("res://scenes/npcs/credits/frames/taevica.png"), "full": preload("res://gui/portraits/taevica.png"), "creator": "Carbonoid"},
	"Neropheus": {"small": preload("res://scenes/npcs/credits/frames/nero.png"), "full": preload("res://gui/portraits/nero.png"), "creator": "Aishishii"},
	"Yuki": {"small": preload("res://scenes/npcs/credits/frames/yuki.png"), "full": preload("res://gui/portraits/yuki.png"), "creator": "MissHolic"},
	"Leopold": {"small": preload("res://scenes/npcs/credits/frames/leopold.png"), "full": preload("res://gui/portraits/leopold.png"), "creator": "Slayernice"},
	"Hatherton": {"small": preload("res://scenes/npcs/credits/frames/hatherton.png"), "full": preload("res://gui/portraits/hatherton.png"), "creator": "N-Jay"},
	"Aethea": {"small": preload("res://scenes/npcs/credits/frames/aethea.png"), "full": preload("res://gui/portraits/aethea.png"), "creator": "kirakiraprince"},
	"Raijin": {"small": preload("res://scenes/npcs/credits/frames/raijin.png"), "full": preload("res://gui/portraits/raijin.png"), "creator": "beelzy"},
	"Diato": {"small": preload("res://scenes/npcs/credits/frames/diato.png"), "full": preload("res://gui/portraits/diato.png"), "creator": "unafkenny"},
	"Jaw": {"small": preload("res://scenes/npcs/credits/frames/jaw.png"), "full": preload("res://gui/portraits/jaw.png"), "creator": "unafkenny"},
	"CHARACTER_POTIONSMASTER": {"small": preload("res://scenes/npcs/credits/frames/potionsmaster.png"), "full": preload("res://gui/portraits/potionsmaster.png"), "creator": "beelzy"}
	}

func _ready():
	dialogues_intern = dialogues.duplicate(true)
	if (character != "" && ProjectSettings.get("npcs_found").has(character)):
		get_node("frame").set_texture(characters[character].small)
		dialogues_intern[0][2] = "DIAG_FRAME1"
		var data = characters[character]
		data.name = character
		if (character == "Pepper"):
			data.name = "Pepper & Carrot"
		dialogues_intern[0][4] = [["KEY_YES", "portrait", data, true], ["KEY_NO", "end"]]
		dialogues_intern[0][6] = tr(data.name)
		dialogues_intern[0][7] = characters[character].creator
