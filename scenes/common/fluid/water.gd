extends Node2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
export var surface_width = 32
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func frame(num):
	get_node("Sprite2").region_rect = Rect2(num * 16, 0, surface_width, 32)
	get_node("Sprite3").region_rect = Rect2(8 * (1 - num), 0, surface_width, 32)
