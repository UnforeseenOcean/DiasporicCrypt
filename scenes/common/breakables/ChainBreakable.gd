# Breaks after a certain number of chain hits

extends "res://scenes/common/breakables/BaseChainBreakable.gd"

export var hp = 5
var max_hp

const CRUMBLE_COLOR = Color(70/255.0, 60/255.0, 77/255.0)

func _ready():
	max_hp = float(hp)
	crumble_color = CRUMBLE_COLOR

func check_chain():
	var chain_counter = chaingui.get_node("chaintext/counterGroup/counter").get_text()
	if (chaingui.is_visible() && chain_counter != counter):
		counter = chain_counter
		hp -= 1
		crumble_color = sprite.get_modulate().linear_interpolate(CRUMBLE_COLOR, 1 - hp/max_hp)
		sprite.set_modulate(crumble_color)
		dust()
	if (hp <= 0):
		is_crumbling = true

func crumble_related():
	for j in related_blocks:
		var related = j.get_ref()
		if (related != null && related.hp > 0):
			related.is_crumbling = true
			related.hp = 0
			related.set("crumble_delay", 10)
			related.start_crumble()


