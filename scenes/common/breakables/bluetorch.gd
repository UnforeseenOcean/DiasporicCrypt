extends "res://scenes/common/breakables/pot.gd"

export var mode = "glow"

func _ready():
	get_node("AnimationPlayer").play(mode)

func _process(delta):
	if (is_crumbling && current_animation == mode):
		current_animation = "break"
	update_animation()
