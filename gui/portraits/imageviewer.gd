extends Node2D

var bg
var container
var overlay

func _ready():
	container = get_node("viewer")
	overlay = get_node("overlay")
	bg = container.get_node("bg")
	set_process(true)
	var back = overlay.get_node("back")
	back.get_node("input").set_text(tr("MAP_BACK"))
	back.set_key("ui_cancel")
	var hide = overlay.get_node("hide")
	hide.get_node("input").set_text(tr("KEY_SHOWHIDE"))
	hide.set_key("ui_select")

func set_image(data):
	container.get_node("portrait").set_texture(data.full)
	bg.set_scale(Vector2(800, container.get_node("portrait").get_texture().get_height()))
	overlay.get_node("title").set_text("\"" + tr(data.name) + "\" " + tr("KEY_FROM") + " " + data.creator)

func enter_screen():
	#set_physics_process(true)
	pass

func exit_screen():
	#set_physics_process(false)
	pass

func _input(event):
	if (event.is_pressed() && event.is_action_pressed("ui_select") && !event.is_echo()):
		overlay.set_visible(!overlay.visible)

func _process(delta):
	if (Input.is_action_pressed("ui_down")):
		var posY = max(container.get_position().y - 3, 592 - container.get_node("portrait").get_texture().get_height())
		container.set_position(Vector2(0, posY))
	elif (Input.is_action_pressed("ui_up")):
		var posY = min(container.get_position().y + 3, 0)
		container.set_position(Vector2(0, posY))

func block_cancel():
	return false
