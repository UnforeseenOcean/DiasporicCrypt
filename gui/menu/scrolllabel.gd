extends Control

# Display correct symbol mapped to controls

var keyboardmap = preload("res://gui/InputCharacters.gd").new()
var up
var down

func _ready():
	up = get_node("up")
	down = get_node("down")
	keyboardmap.update_keys()
	up.set_text(keyboardmap.map_action("ui_up"))
	down.set_text(keyboardmap.map_action("ui_down"))

func reload_key():
	keyboardmap.update_keys()
	up.set_text(keyboardmap.map_action("ui_up"))
	down.set_text(keyboardmap.map_action("ui_down"))

func _notification(what):
	if what == NOTIFICATION_PREDELETE && keyboardmap:
		keyboardmap.free()
