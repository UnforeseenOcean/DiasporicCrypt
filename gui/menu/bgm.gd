extends Control

var scrollrange
var list
var music
var title
var sfx
var sfxclass = preload("res://gui/menu/sfx.tscn")

var itemclass = preload("res://gui/menu/textitem.tscn")
var tracks = [
	{"title": "Menu Screen", "bgm": preload("res://gui/global/global.ogg")},
	{"title": "Catacombs", "bgm": preload("res://levels/common/catacombs.ogg")},
	{"title": "Rock Vivace", "bgm": preload("res://levels/common/BGM2.ogg")},
	{"title": "Yellowing Grimoires", "bgm": preload("res://levels/common/BGM1.ogg")},
	{"title": "Vampire Means Business", "bgm": preload("res://levels/common/BGM4.ogg")},
	{"title": "Forest", "bgm": preload("res://levels/forest/BGM1.ogg")},
	{"title": "Training", "bgm": preload("res://levels/common/BGM3.ogg")}
]

func _ready():
	sfx = sfxclass.instance()
	add_child(sfx)
	music = get_tree().get_root().get_node("world/music")
	title = get_node("current/currentbgm")
	list = get_node("list")
	var listcontainer = list.get_node("container")
	scrollrange = list.get_size()
	set_process_input(true)
	var back = get_node("back")
	back.get_node("input").set_text(tr("MAP_BACK"))
	back.set_key("ui_cancel")
	for i in range(0, tracks.size()):
		var item_obj = itemclass.instance()
		var data = tracks[i]
		item_obj.set_name(data.title)
		item_obj.set_text(data.title)
		# don't lose focus on irrelevant inputs
		item_obj.set_focus_neighbour(MARGIN_LEFT, ".")
		item_obj.set_focus_neighbour(MARGIN_RIGHT, ".")
		item_obj.set_focus_neighbour(MARGIN_BOTTOM, ".")
		# previous item should focus the next item properly
		if (listcontainer.get_child_count() > 0):
			var lastitem = listcontainer.get_child(listcontainer.get_child_count() - 1)
			lastitem.set_focus_neighbour(MARGIN_BOTTOM, "")
		listcontainer.add_child(item_obj)
	if (listcontainer.get_child_count() > 0):
		listcontainer.get_child(0).set_focus_neighbour(MARGIN_TOP, ".")
	var index = -1
	var focusindex = 0
	if (ProjectSettings.get("bgmselection") != null):
		index = ProjectSettings.get("bgmselection")
		focusindex = index
	if (index >= 0):
		title.set_text(tracks[index].title)
	else:
		title.set_text("(" + tr("KEY_NOMUSIC") + ")")
	listcontainer.get_child(focusindex).grab_focus()

func check_scroll():
	var item = get_focus_owner()
	var vscroll = get_node("list").get_v_scroll()
	var itempos = item.get_position().y
	var itemsize = item.get_size().y
	if (vscroll > itempos || vscroll + scrollrange.y < itempos + itemsize):
		get_node("list").set_v_scroll(itempos)

func _input(event):
	if (event.is_pressed() && !event.is_echo()):
		var focus = get_focus_owner()
		if (event.is_action_pressed("ui_accept")):
			var index = focus.get_index()
			if (ProjectSettings.get("bgmselection") != index):
				var track = tracks[index].bgm
				ProjectSettings.set("bgmselection", index)
				music.set_stream(track)
				music.play()
				title.set_text(tracks[index].title)
				sfx.get_node("confirm").play()

func block_cancel():
	return false
